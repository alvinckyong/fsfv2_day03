np/**
 * Created by alvinyong on 29/6/16.
 */

var express = require("express");

var app = express();

app.use(express.static(__dirname +"/public"));
app.use(express.static(__dirname +"/bower_components"));

app.use(function(req, res) {
    console.info("File not found in public: %s" + req.originalUrl);
    res.redirect("/error.html");


app.set("port",
    process.argv[2] || process.env.APP_PORT || 3000);

app.listen(app.get("port"), function() {
    console.info("Application started on port %d", app.get("port"));